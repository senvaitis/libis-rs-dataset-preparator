# libis-rs-dataset-preparator
This repo is meant to be private and forbidden from usage without written permission from Kazimieras Senvaitis and Martynas Mažvydas National Library of Lithuania (LNB).

This project is responsible for preparation of datasets for libis recommendation system research & development.

## Instructions (fresh start)
### Prepare raw data database
1. Have PostgreSQL DB installed (tested with PostgreSQL 12).
2. Put PostgreSQL Client (psql.exe) in path (test by running "psql" with Windows PowerShell).
3. Extract raw CSV data (requires knowledge of passwords, hostnames, ports).
4. Prepare raw data database.

### Prepare and export nK-dataset
1. Must perform raw data database preparations.
2. Prepare nK-dataset database.
3. Extract nK-dataset CSV data.