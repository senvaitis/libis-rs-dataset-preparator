psql -h <HOST> -p <PORT> -U <USER> --password -d <DATABASE> -c  "\COPY (
  select
    ps.id,
    ps.added_to_shelf_timestamp,
    ps.bibliographic_record_id,
    ps.person_shelf_id
  from person_shelf_bibliographic_record ps
) TO '<PATH-IN-LOCAL-FILESYSTEM>\u.person-shelf' DELIMITER ',' CSV HEADER;"
Read-Host -Prompt "Press Enter to exit"