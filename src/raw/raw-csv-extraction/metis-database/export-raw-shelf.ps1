psql -h <HOST> -p <PORT> -U <USER> --password -d <DATABASE> -c  "\COPY (
  select
    s.id,
    s.default_shelf,
    s.title,
    s.person_id
  from person_shelf s
) TO '<PATH-IN-LOCAL-FILESYSTEM>\u.shelf' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"
Read-Host -Prompt "Press Enter to exit"