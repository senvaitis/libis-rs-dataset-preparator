psql -h <HOST> -p <PORT> -U <USER> --password -d <DATABASE> -c  "\COPY (
  select
    p.id,
    p.libis_id
  from person p
) TO '<PATH-IN-LOCAL-FILESYSTEM>\u.person' DELIMITER ',' CSV HEADER;"
Read-Host -Prompt "Press Enter to exit"