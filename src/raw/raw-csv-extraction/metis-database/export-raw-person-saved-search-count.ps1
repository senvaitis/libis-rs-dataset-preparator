psql -h <HOST> -p <PORT> -U <USER> --password -d <DATABASE> -c  "\COPY (
  select
    count(*)
  from bibliographic_record_saved_search ss
) TO '<PATH-IN-LOCAL-FILESYSTEM>\u.saved-search-count' DELIMITER ',' CSV HEADER;"
Read-Host -Prompt "Press Enter to exit"