psql -h <HOST> -p <PORT> -U <USER> --password -d <DATABASE> -c  "\COPY (
  select
    r.id,
    r.person_id,
    r.record_id,
    r.rating
  from bibliographic_record_rating r
) TO '<PATH-IN-LOCAL-FILESYSTEM>\u.rating' DELIMITER ',' CSV HEADER;"
Read-Host -Prompt "Press Enter to exit"