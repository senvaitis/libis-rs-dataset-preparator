psql -h bis -p 5432 -U libis_upd --password -d libis -c  "\COPY (
    select
      u.id,
      date_part('year', age(now()::date, u.birth_date::date)) as age,
      c_edu.code as education,
      u.gender,
      rg.code as reader_group,
      c_ict.code as identification_code_type,
      u.is_currently_studying,
      c_occ.code as occupation,
      coalesce(c_tra_country.code, c_ra_country.code) as country,
      coalesce(c_tra_county.code, c_ra_county.code) as county
    from libis_adm.users u
    left join libis_adm.addresses ra on ra.id = u.residence_address_id
    left join libis_adm.addresses tra on tra.id = u.temporary_residence_address_id
    left join libis_adm.standard_classifier_entries c_edu on c_edu.id = u.education_id
    left join libis_adm.standard_classifier_entries c_ict on c_ict.id = u.identification_code_type_id
    left join libis_adm.standard_classifier_entries c_occ on c_occ.id = u.occupation_id
    left join libis_adm.standard_classifier_entries c_ra_country on c_ra_country.id = ra.country_id
    left join libis_adm.standard_classifier_entries c_ra_county on c_ra_county.id = ra.county_id
    left join libis_adm.standard_classifier_entries c_tra_country on c_tra_country.id = tra.country_id
    left join libis_adm.standard_classifier_entries c_tra_county on c_tra_county.id = tra.county_id
    left join libis_adm.reader_groups rg on rg.id = u.global_reader_group_id
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\raw\u.user' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"
Read-Host -Prompt "Press Enter to exit"