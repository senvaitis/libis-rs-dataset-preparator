psql -h bis -p 5432 -U libis_upd --password -d libis -c  "\COPY (
    select distinct sce.code, sce.title
    from libis_adm.users u
    left join libis_adm.standard_classifier_entries sce on sce.id = u.identification_code_type_id
    where sce.code is not null
    order by sce.code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\raw\u.identification-code-type' DELIMITER ',' CSV HEADER;"
Read-Host -Prompt "Press Enter to exit"