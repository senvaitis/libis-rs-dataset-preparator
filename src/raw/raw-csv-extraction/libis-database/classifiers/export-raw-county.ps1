psql -h bis -p 5432 -U libis_upd --password -d libis -c  "\COPY (
    select code, title from (
        select distinct sce.code, sce.title
        from libis_adm.users u
        left join libis_adm.addresses ra on ra.id = u.residence_address_id
        left join libis_adm.standard_classifier_entries sce on sce.id = ra.county_id
        where sce.code is not null
        union
        select distinct sce.code, sce.title
        from libis_adm.users u
        left join libis_adm.addresses tra on tra.id = u.temporary_residence_address_id
        left join libis_adm.standard_classifier_entries sce on sce.id = tra.county_id
        where sce.code is not null
    ) a order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\raw\u.county' DELIMITER ',' CSV HEADER;"
Read-Host -Prompt "Press Enter to exit"