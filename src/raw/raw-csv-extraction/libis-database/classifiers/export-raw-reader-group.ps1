psql -h bis -p 5432 -U libis_upd --password -d libis -c  "\COPY (
    select distinct rg.code, rg.name
    from libis_adm.users u
    left join libis_adm.reader_groups rg on rg.id = u.global_reader_group_id
    where rg.code is not null
    order by rg.code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\raw\u.reader-group' DELIMITER ',' CSV HEADER;"
Read-Host -Prompt "Press Enter to exit"