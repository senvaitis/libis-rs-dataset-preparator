psql -h bis -p 5432 -U libis_upd --password -d libis -c  "\COPY (
  select
    so_all.id,
    rt.user_id,
    so_all.record_id,
    so_all.creation_date_time
  from libis_adm.service_order_all so_all
  left join libis_adm.reader_tickets rt on rt.id = so_all.reader_ticket_id
  where 1=1
    and rt.user_id is not null
    and so_all.record_id is not null
    and so_all.creation_date_time is not null
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\raw\u.order' DELIMITER ',' CSV HEADER;"
Read-Host -Prompt "Press Enter to exit"