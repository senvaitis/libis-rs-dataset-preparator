1. Install PostgreSQL with default settings
3. run 'run-migrations.ps1'
4. run 'import-users.ps1'
5. run 'import-records.ps1'
6. run 'import-orders-BEFORE.ps1'
7. run 'import-orders.ps1'
8. run 'import-orders-AFTER.ps1'

Fix: psql client encoding:
https://stackoverflow.com/a/67625652/4726792
SET client_encoding = 'UTF8';