select * from libis_adm.service_order_all
order by creation_date_time asc
limit 1000;

select count(*) from libis_adm.service_order_all;

select
    date_trunc('year', creation_date_time) as year,
    count(*)
from libis_adm.service_order_all
group by year;

select extract(year from creation_date_time) as yyyy,
       count(*)
from libis_adm.service_order_all
group by 1;

select user_id, record_id, count(*)
from libis_raw.orders
group by user_id, record_id
having count(*) > 1
order by count(*) desc;

-- bendra suma užsakymų, kurie pasikartoja
select sum(repeated_orders) from (
    select user_id, record_id, count(*) as repeated_orders
    from libis_raw.orders
    group by user_id, record_id
    having count(*) > 1 and count(*) < 30
    order by count(*) desc
) a;


-- userių kiekis, kurie perskaito tą pačią knygą 2 kartus
select count(distinct user_id) from (
    select user_id, record_id, count(*) as repeated_orders
    from libis_raw.orders
    group by user_id, record_id
    having count(*) = 2
    order by count(*) desc
) a;
-- daugiausiai užsakymų turėję useriai
select user_id, count(*)
from libis_raw.orders
group by user_id
order by count(*) desc;

select count(*)
from libis_raw.orders
group by user_id, record_id
having count(*) > 1
order by count(*);

select count(distinct record_id)
from libis_raw.orders;

select count(distinct user_id)
from libis_raw.orders;


select count(distinct record_id)
from libis_raw_am0.orders;

select count(*)
from libis_raw.records
where substr(leader, 7, 3) = 'am0';


-- kiek užsakymų tam tikrame periode
select count(*)
from libis_raw_am0.orders o
where o.creation_date_time >= '2021-07-01';