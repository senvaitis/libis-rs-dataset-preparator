select * from libis_adm.users where id = 6860764;
select * from libis_adm.users where id = 7032112;
select * from libis_adm.users where id = 6843466;

select * from libis_adm.users where first_name = 'Kazimieras' and last_name = 'Senvaitis';

-- which books did the user read
select o.creation_date_time, o.record_id, r.title_for_list from libis_raw.orders o
left join libis_raw.records r on r.id = o.record_id
where 1=1
--  and substr(r.leader, 7, 3) = 'am0'
  and o.user_id = 6858078
order by creation_date_time desc;


-- how many books did the user read
select count(*) from libis_raw.orders o
left join libis_raw.records r on r.id = o.record_id
where 1=1
--  and substr(r.leader, 7, 3) = 'am0'
  and o.user_id = 6858078;

select count(*) from ( -- 45k
    select user_id from libis_raw.orders o
--    left join libis_raw.records r on r.id = o.record_id
--    where substr(r.leader, 7, 3) = 'am0'
    group by user_id
    having count(user_id) = 1
--    order by user_id asc
) a
;

select user_id
from libis_raw.orders o -- kai
--    left join libis_raw.records r on r.id = o.record_id
--    where substr(r.leader, 7, 3) = 'am0'
    group by user_id
    having count(user_id) = 1
--    order by user_id asc

select count(*) from ( -- 17k, must be less than 45k
    select distinct record_id from libis_raw.orders o
    left join libis_raw.records r on r.id = o.record_id
    where substr(r.leader, 7, 3) = 'am0'
      and user_id in (
        select user_id from libis_raw.orders
        group by user_id
        having count(*) = 1
        order by count(*), user_id desc
    )
) a ;

select count(*) from (
    select o.id, o.creation_date_time, o.user_id, o.record_id, user_read_count.user_alltime_book_count
    from libis_raw.orders o
    join libis_40k_anti.records r on r.libis_id = o.record_id
    join (
        select user_id, count(*) as user_alltime_book_count from libis_raw.orders o
        join libis_40k_anti.records r on r.libis_id = o.record_id
        group by user_id
        having count(*) = 1
        order by count(*), user_id desc
    ) user_read_count on user_read_count.user_id = o.user_id
    order by o.creation_date_time desc, o.id desc
) a;