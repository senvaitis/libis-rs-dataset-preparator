CREATE TABLE libis_raw.users (
  id SERIAL not null,
  age int4,
  gender VARCHAR(50),
  is_currently_studying boolean,
  education VARCHAR(50),
  occupation VARCHAR(50),
  country VARCHAR(50),
  county VARCHAR(50),
  identification_code_type VARCHAR(50),
  reader_group VARCHAR(50),
  PRIMARY KEY (id)
);

CREATE TABLE libis_raw.orders (
  id SERIAL not null,
  creation_date_time timestamp,
  user_id int8 not null,
  record_id int8 not null,
  PRIMARY KEY (id)
);

CREATE TABLE libis_raw.records (
  id SERIAL not null,
  code VARCHAR(50),
  leader VARCHAR(50),
  title_for_list VARCHAR(4000),
  release_date int4,
  PRIMARY KEY (id)
);

CREATE TABLE libis_raw.record_genres (
  record_id int8,
  solr_id VARCHAR(50),
  udk VARCHAR(2000),
  genre VARCHAR(400),
 PRIMARY KEY (solr_id)
);

CREATE TABLE libis_raw.record_release_dates (
  record_id int8,
  solr_id VARCHAR(50),
  raw_release_date VARCHAR(4),
  release_date int4,
 PRIMARY KEY (solr_id)
);

CREATE TABLE libis_raw.record_resource_languages (
  record_id int8,
  solr_id VARCHAR(50),
  resource_language VARCHAR(3),
 PRIMARY KEY (solr_id)
);

CREATE TABLE libis_raw.country (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

CREATE TABLE libis_raw.county (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

CREATE TABLE libis_raw.education (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

CREATE TABLE libis_raw.identification_code_type (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

CREATE TABLE libis_raw.occupation (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

CREATE TABLE libis_raw.reader_group (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

alter table if exists libis_raw.orders add constraint fk_orders_user_id foreign key (user_id) references libis_raw.users;
alter table if exists libis_raw.orders add constraint fk_orders_record_id foreign key (record_id) references libis_raw.records;

CREATE INDEX orders_user_index ON libis_raw.orders(user_id);
CREATE INDEX orders_record_index ON libis_raw.orders(record_id);

alter table if exists libis_raw.record_genres add constraint fk_record_genres_record_id foreign key (record_id) references libis_raw.records;
