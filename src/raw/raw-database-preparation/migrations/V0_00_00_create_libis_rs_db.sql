-- Execute in psql, e.g.:
-- "C:\Program Files\PostgreSQL\11\bin\psql" -U postgres -f "<path to LIBISv6>\libis-back-office\install\develop\setup-dev-db.sql"
-- In case of manual execution using pgAdmin4, run the following from "postgres" database "Query Tool..."
DROP DATABASE IF EXISTS libis_rs;
-- TODO collation: CREATE DATABASE libis_rs WITH OWNER = postgres TEMPLATE template0 ENCODING = 'UTF8' LC_COLLATE 'lt_LT' LC_CTYPE 'lt_LT' CONNECTION LIMIT = -1;
CREATE DATABASE libis_rs WITH OWNER = postgres CONNECTION LIMIT = -1;

-- In case of manual execution using pgAdmin4, run the following from "libis_rs" database "Query Tool..."
\connect libis_rs;

CREATE SCHEMA libis_raw;
CREATE SCHEMA libis_100k;

-- CREATE EXTENSION IF NOT EXISTS btree_gist SCHEMA pg_catalog;
-- CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA pg_catalog;
-- in libis database you can see schemas: 'libis_jbi', 'libis_nai', 'libis_adm', test connection to database in pgAdmin by creating server 'local_libis_it' to 'libis' database with user 'libis_it'.
