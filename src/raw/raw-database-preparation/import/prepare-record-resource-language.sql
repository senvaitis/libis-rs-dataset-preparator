delete from libis_raw.record_resource_languages
where cast(resource_language !~ '^[a-z]+$' as boolean);

update libis_raw.record_resource_languages
set record_id = split_part(solr_id, '-', 1)::int8;

--select count(*) from libis_raw.record_resource_languages
--
--select * from libis_raw.record_resource_languages
--where cast(raw_resource_language !~ '^[a-z]+$' as boolean);