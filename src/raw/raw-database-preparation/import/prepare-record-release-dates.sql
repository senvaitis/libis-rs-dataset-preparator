delete from libis_raw.record_release_dates
where cast(raw_release_date !~ '^\d+$' as boolean);

update libis_raw.record_release_dates
set record_id = split_part(solr_id, '-', 1)::int8,
    release_date = cast (raw_release_date as int4);

UPDATE libis_raw.records r
SET release_date=rrd.release_date
FROM libis_raw.record_release_dates AS rrd
WHERE r.id=rrd.record_id;
