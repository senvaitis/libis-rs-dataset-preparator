DROP INDEX libis_raw.orders_user_index, libis_raw.orders_record_index;

ALTER TABLE libis_raw.orders DROP CONSTRAINT fk_orders_record_id;
ALTER TABLE libis_raw.orders DROP CONSTRAINT fk_orders_user_id;

ALTER TABLE libis_raw.orders DISABLE TRIGGER ALL;
--commit;