select count(*), 'country' from libis_raw.country
union all
select count(*), 'county' from libis_raw.county
union all
select count(*), 'education' from libis_raw.education
union all
select count(*), 'identification_code_type' from libis_raw.identification_code_type
union all
select count(*), 'occupation' from libis_raw.occupation
union all
select count(*), 'orders' from libis_raw.orders
union all
select count(*), 'reader_group' from libis_raw.reader_group
union all
select count(*), 'record_genres' from libis_raw.record_genres
union all
select count(*), 'record_release_dates' from libis_raw.record_release_dates
union all
select count(*), 'record_resource_languages' from libis_raw.record_resource_languages
union all
select count(*), 'records' from libis_raw.records
union all
select count(*), 'users' from libis_raw.users;