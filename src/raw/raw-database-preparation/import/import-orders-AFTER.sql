alter table if exists libis_raw.orders add constraint fk_orders_user_id foreign key (user_id) references libis_raw.users;
alter table if exists libis_raw.orders add constraint fk_orders_record_id foreign key (record_id) references libis_raw.records;

CREATE INDEX orders_user_index ON libis_raw.orders(user_id);
CREATE INDEX orders_record_index ON libis_raw.orders(record_id);

ALTER TABLE libis_raw.orders ENABLE TRIGGER ALL;
