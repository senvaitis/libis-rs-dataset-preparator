psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        id,
        code,
        leader,
        title_for_list as title,
        string_agg(rg.genre, '|') as genres
    from libis_20m.records r
    left join libis_20m.record_genres rg on rg.record_id = r.id
    group by r.id
    order by r.id asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\20m\u.record-piped' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"
Read-Host -Prompt "Press Enter to exit"