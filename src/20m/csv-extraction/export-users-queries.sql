select
    id,
    age_group,
    country,
    county,
    education,
    gender,
    identification_code_type,
    is_currently_studying,
    occupation,
    reader_group
from libis_20m.users u;