-- Based on:
-- https://stackoverflow.com/a/45621447/4726792
-- https://stackoverflow.com/a/56068467/4726792
SELECT STRING_AGG(FORMAT('count(case when rg.genre=''%s'' then 1 end) %s', rg.genre, regexp_replace(regexp_replace(lower(rg.genre), '[\.,-:]', '', 'g'), ' ', '_', 'g')), ','||E'\n')
FROM (
  SELECT DISTINCT genre
  FROM libis_20m.record_genres
  ORDER BY 1
) rg;

select
    id,
    code,
    leader,
    count(case when rg.genre='Bendrasis skyrius' then 1 end) bendrasis_skyrius,
    count(case when rg.genre='Biografijos' then 1 end) biografijos,
    count(case when rg.genre='Demografija. Sociologija. Statistika' then 1 end) demografija_sociologija_statistika,
    count(case when rg.genre='Etnografija. Papročiai. Tautosaka' then 1 end) etnografija_papročiai_tautosaka,
    count(case when rg.genre='Filosofija, psichologija' then 1 end) filosofija_psichologija,
    count(case when rg.genre='Gamtos mokslai' then 1 end) gamtos_mokslai,
    count(case when rg.genre='Geografija' then 1 end) geografija,
    count(case when rg.genre='Grafika. Graviūra' then 1 end) grafika_graviūra,
    count(case when rg.genre='Grožinė literatūra' then 1 end) grožinė_literatūra,
    count(case when rg.genre='Grožinė literatūra vaikams ir jaunimui' then 1 end) grožinė_literatūra_vaikams_ir_jaunimui,
    count(case when rg.genre='Įmonių valdymas. Gamybos, prekybos ir transporto organizavimas' then 1 end) įmonių_valdymas_gamybos_prekybos_ir_transporto_organizavimas,
    count(case when rg.genre='Istorija' then 1 end) istorija,
    count(case when rg.genre='Kalbotyra' then 1 end) kalbotyra,
    count(case when rg.genre='Kino menas. Masinės šventės' then 1 end) kino_menas_masinės_šventės,
    count(case when rg.genre='Literatūros kritika ir literatūros mokslas' then 1 end) literatūros_kritika_ir_literatūros_mokslas,
    count(case when rg.genre='Medicina' then 1 end) medicina,
    count(case when rg.genre='Namų ūkis. Komunalinis ūkis. Buities tarnyba' then 1 end) namų_ūkis_komunalinis_ūkis_buities_tarnyba,
    count(case when rg.genre='Pažintinė literatūra vaikams' then 1 end) pažintinė_literatūra_vaikams,
    count(case when rg.genre='Piešimas. Taikomoji dekoratyvinė dailė. Dizainas' then 1 end) piešimas_taikomoji_dekoratyvinė_dailė_dizainas,
    count(case when rg.genre='Politika' then 1 end) politika,
    count(case when rg.genre='Religija, teologija' then 1 end) religija_teologija,
    count(case when rg.genre='Socialinis aprūpinimas ir socialinė pagalba. Draudimas' then 1 end) socialinis_aprūpinimas_ir_socialinė_pagalba_draudimas,
    count(case when rg.genre='Šventės. Choreografija' then 1 end) šventės_choreografija,
    count(case when rg.genre='Švietimas. Ugdymas. Mokymas. Laisvalaikio organizavimas' then 1 end) švietimas_ugdymas_mokymas_laisvalaikio_organizavimas,
    count(case when rg.genre='Teatras. Scenos menas' then 1 end) teatras_scenos_menas,
    count(case when rg.genre='Teisė. Teisės mokslai' then 1 end) teisė_teisės_mokslai,
    count(case when rg.genre='Valstybės administracinis valdymas. Karyba' then 1 end) valstybės_administracinis_valdymas_karyba,
    count(case when rg.genre='Visuomenės mokslų teorija ir metodai' then 1 end) visuomenės_mokslų_teorija_ir_metodai,
    count(case when rg.genre='Zoologija' then 1 end) zoologija,
    count(case when rg.genre='Žaidimai. Sportas' then 1 end) žaidimai_sportas
left join libis_20m.record_genres rg on rg.record_id = r.id
group by r.id
order by r.id asc;

SHOW SERVER_ENCODING;



select
    id,
    age_group,
    country.title country,
    county.title county,
    education.title education,
    gender,
    identification_code_type.title identification_code_type,
    is_currently_studying,
    occupation.title occupation,
    reader_group.title reader_group
from libis_20m.users u
left join libis_20m.country country on u.country = country.code
left join libis_20m.county county on u.county = county.code
left join libis_20m.education education on u.education = education.code
left join libis_20m.occupation occupation on u.occupation = occupation.code
left join libis_20m.identification_code_type identification_code_type on u.identification_code_type = identification_code_type.code
left join libis_20m.reader_group reader_group on u.reader_group = reader_group.code
order by id asc;

select
    o.id,
    o.creation_date_time,
    o.record_id,
    o.user_id
from libis_20m.orders o
order by id asc;