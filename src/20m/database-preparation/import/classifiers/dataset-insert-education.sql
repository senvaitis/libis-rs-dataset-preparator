insert into libis_20m.education (
    code,
    title)
select
    e.code,
    e.title
from libis_raw_am0.education e
where e.code in (
    select distinct u.education
    from libis_20m.users u
    where u.education is not null
)
order by e.code asc;
