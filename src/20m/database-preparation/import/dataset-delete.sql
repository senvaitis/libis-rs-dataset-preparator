delete from libis_20m.country;
delete from libis_20m.county;
delete from libis_20m.education;
delete from libis_20m.identification_code_type;
delete from libis_20m.occupation;
delete from libis_20m.reader_group;

TRUNCATE TABLE libis_20m.orders RESTART IDENTITY;
TRUNCATE TABLE libis_20m.record_genres RESTART IDENTITY;
TRUNCATE TABLE libis_20m.record_resource_languages RESTART IDENTITY;
TRUNCATE TABLE libis_20m.records RESTART IDENTITY;
TRUNCATE TABLE libis_20m.users RESTART IDENTITY;