insert into libis_20m.record_genres (
    record_id,
    genre)
select distinct
    r.id,
    rg.genre
from libis_raw_am0.record_genres rg
join libis_20m.records r on r.libis_id = rg.record_id;
