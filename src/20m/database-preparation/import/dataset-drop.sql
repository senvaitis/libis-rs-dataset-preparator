drop table libis_20m.country;
drop table libis_20m.county;
drop table libis_20m.education;
drop table libis_20m.identification_code_type;
drop table libis_20m.occupation;
drop table libis_20m.reader_group;


drop table libis_20m.orders;
drop table libis_20m.record_genres;
drop table libis_20m.record_resource_languages;
drop table libis_20m.records;
drop table libis_20m.users;