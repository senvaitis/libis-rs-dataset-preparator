insert into libis_20m.record_resource_languages (
    record_id,
    resource_language)
select distinct
    r.id,
    rg.resource_language
from libis_raw_am0.record_resource_languages rg
join libis_20m.records r on r.libis_id = rg.record_id;
