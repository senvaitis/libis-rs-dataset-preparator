insert into libis_raw_am0.orders (creation_date_time, record_id, user_id)
select
    o.creation_date_time,
    o.record_id,
    o.user_id
from libis_raw.orders o
join libis_raw_am0.records r on r.id = o.record_id
--where o.record_id in (
--    select id from libis_raw_am0.records
--)
order by o.creation_date_time desc;

--select count(*) from libis_raw_am0.records;