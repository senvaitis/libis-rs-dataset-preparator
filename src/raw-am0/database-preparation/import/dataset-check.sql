select count(*), 'country' from libis_raw_am0.country
union all
select count(*), 'county' from libis_raw_am0.county
union all
select count(*), 'education' from libis_raw_am0.education
union all
select count(*), 'identification_code_type' from libis_raw_am0.identification_code_type
union all
select count(*), 'occupation' from libis_raw_am0.occupation
union all
select count(*), 'orders' from libis_raw_am0.orders
union all
select count(*), 'reader_group' from libis_raw_am0.reader_group
union all
select count(*), 'record_genres' from libis_raw_am0.record_genres
union all
select count(*), 'record_resource_languages' from libis_raw_am0.record_resource_languages
union all
select count(*), 'records' from libis_raw_am0.records
union all
select count(*), 'users' from libis_raw_am0.users;

-- ranked most reading users
select user_id, count(*) from libis_raw_am0.orders o
group by user_id
order by count(*) desc;

-- verify record count from orders perspective
select count(*) from (
    select distinct record_id from libis_raw_am0.orders
) a;

-- verify user count from orders perspective
select count(*) from (
    select distinct user_id from libis_raw_am0.orders
) a;

-- ranked most read books
select record_id, r.title_for_list, count(*) from libis_raw_am0.orders o
left join libis_raw_am0.records r on r.id = o.record_id
group by record_id, r.title_for_list
order by count(*) desc;

select count(*) from libis_raw.users
where 1=1
--    and country is null
--    and county is null
--    and education is null
--    and identification_code_type is null
--    and occupation is null
    and reader_group is null
;