drop table libis_raw_am0.country;
drop table libis_raw_am0.county;
drop table libis_raw_am0.education;
drop table libis_raw_am0.identification_code_type;
drop table libis_raw_am0.occupation;
drop table libis_raw_am0.reader_group;

drop table libis_raw_am0.orders;
drop table libis_raw_am0.record_genres;
drop table libis_raw_am0.records;
drop table libis_raw_am0.users;
drop table libis_raw_am0.record_resource_languages;