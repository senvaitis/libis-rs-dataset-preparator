insert into libis_raw_am0.reader_group (
    code,
    title)
select
    e.code,
    e.title
from libis_raw.reader_group e
where e.code in (
    select distinct u.reader_group
    from libis_raw_am0.users u
    where u.reader_group is not null
)
order by e.code asc;
