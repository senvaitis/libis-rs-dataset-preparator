insert into libis_raw_am0.county (
    code,
    title)
select
    e.code,
    e.title
from libis_raw.county e
where e.code in (
    select distinct u.county
    from libis_raw_am0.users u
    where u.county is not null
)
order by e.code asc;
