insert into libis_raw_am0.occupation (
    code,
    title)
select
    e.code,
    e.title
from libis_raw.occupation e
where e.code in (
    select distinct u.occupation
    from libis_raw_am0.users u
    where u.occupation is not null
)
order by e.code asc;
