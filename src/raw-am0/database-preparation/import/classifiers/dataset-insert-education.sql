insert into libis_raw_am0.education (
    code,
    title)
select
    e.code,
    e.title
from libis_raw.education e
where e.code in (
    select distinct u.education
    from libis_raw_am0.users u
    where u.education is not null
)
order by e.code asc;
