insert into libis_raw_am0.users (
    id,
    age,
    country,
    county,
    education,
    gender,
    identification_code_type,
    is_currently_studying,
    occupation,
    reader_group
)
select
    u.id,
    u.age,
    u.country,
    u.county,
    u.education,
    u.gender,
    u.identification_code_type,
    u.is_currently_studying,
    u.occupation,
    u.reader_group
from libis_raw.users u
join (select distinct user_id from libis_raw_am0.orders) o on o.user_id = u.id;


--select
--    count(*)
--from libis_raw.users u
--join libis_raw_am0.orders o on o.user_id = u.id;
--
--select
--    count(*)
--from libis_raw.users u
--join (select distinct user_id from libis_raw_am0.orders) o on o.user_id = u.id;
--
--select
--    count(*) from (
--select distinct user_id from libis_raw_am0.orders
--) a;