insert into libis_raw_am0.record_resource_languages (
    record_id,
    resource_language)
select distinct
    r.id,
    rg.resource_language
from libis_raw.record_resource_languages rg
join libis_raw_am0.records r on r.id = rg.record_id;
