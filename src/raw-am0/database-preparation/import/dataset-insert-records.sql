-- 9000 most popular books (am0)
insert into libis_raw_am0.records (
    id,
    code,
    leader,
    title_for_list,
    release_date)
select
    r.id,
    r.code,
    r.leader,
    r.title_for_list,
    r.release_date
from libis_raw.records r
where substr(r.leader, 7, 3) = 'am0';

--select count(*) from libis_raw_am0.records where release_date is not null;
--select count(*) from libis_raw_am0.records where release_date is null;
