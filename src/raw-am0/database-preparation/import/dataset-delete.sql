delete from libis_raw_am0.country;
delete from libis_raw_am0.county;
delete from libis_raw_am0.education;
delete from libis_raw_am0.identification_code_type;
delete from libis_raw_am0.occupation;
delete from libis_raw_am0.reader_group;

delete from libis_raw_am0.orders;
delete from libis_raw_am0.record_genres;
delete from libis_raw_am0.records;
delete from libis_raw_am0.users;
delete from libis_raw_am0.record_resource_languages;