--am0 record count
select count(distinct record_id)
from libis_raw_am0.orders;


--Users with at least 1 book order. Users of public library.
select count(distinct user_id)
from libis_raw_am0.orders;

--Users with at least 20 book order. Users of public library.
select count(*) from (
    select user_id
    from libis_raw_am0.orders
    group by user_id
    having count(*) >= 20
) a;

select count(*)
from libis_raw_am0.users;

SELECT
    sum(CASE WHEN age_group IS NOT NULL THEN 1 ELSE 0 END) / 659670.0 AS "Age group",
    sum(CASE WHEN country IS NOT NULL THEN 1 ELSE 0 END) / 659670.0 AS "Country",
    sum(CASE WHEN county IS NOT NULL THEN 1 ELSE 0 END) / 659670.0 AS "County",
    sum(CASE WHEN education IS NOT NULL THEN 1 ELSE 0 END) / 659670.0 AS "Education",
    sum(CASE WHEN gender IS NOT NULL THEN 1 ELSE 0 END) / 659670.0 AS "Gender",
    sum(CASE WHEN identification_code_type IS NOT NULL THEN 1 ELSE 0 END) / 659670.0 AS "Identification code type",
    sum(CASE WHEN is_currently_studying IS NOT NULL THEN 1 ELSE 0 END) / 659670.0 AS "Is currently studying",
    sum(CASE WHEN occupation IS NOT NULL THEN 1 ELSE 0 END) / 659670.0 AS "Occupation",
    sum(CASE WHEN reader_group IS NOT NULL THEN 1 ELSE 0 END) / 659670.0 AS "Reader group"
FROM libis_raw_am0.users;

select count(*)
FROM libis_raw_am0.users
where age_group IS NULL;

select * from libis_raw_am0.users;

select count(*) from libis_raw_am0.orders; -- 26346275
-- repeated read count per user per record
select
    sum(CASE WHEN read_count = 1 THEN 1 ELSE 0 END) AS "1",
    sum(CASE WHEN read_count = 2 THEN 1 ELSE 0 END) AS "2",
    sum(CASE WHEN read_count = 3 THEN 1 ELSE 0 END) AS "3",
    sum(CASE WHEN read_count >= 4 and read_count < 10 THEN 1 ELSE 0 END) AS "4-10",
    sum(CASE WHEN read_count >= 10 THEN 1 ELSE 0 END) AS "10+"
from (
    select
        user_id, record_id, count(*) as read_count
    from libis_raw_am0.orders o
    group by o.user_id, o.record_id
    order by count(*) desc
) a
;