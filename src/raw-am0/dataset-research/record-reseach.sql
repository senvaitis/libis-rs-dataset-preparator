select * from libis_jbi.records where id = 6269906;
select * from libis_jbi.records where id = 1225710;


select id, count(*)
from libis_raw.records
group by id
having count(*) > 1

-- įrašai turintys bent vieną perskaitymą pagal žanrų skaičių
select count(*) from libis_raw_am0.records; -- 1107587
select
--    sum(CASE WHEN genre_count > 2 THEN 1 ELSE 0 END) / 1107587.0 AS "3+",
--    sum(CASE WHEN genre_count > 1 THEN 1 ELSE 0 END) / 1107587.0 AS "2",
--    sum(CASE WHEN genre_count > 0 THEN 1 ELSE 0 END) / 1107587.0 AS "1"
    sum(CASE WHEN genre_count > 0 and genre_count <= 1 THEN 1 ELSE 0 END) AS "1", -- 827116
    sum(CASE WHEN genre_count > 1 and genre_count <= 2 THEN 1 ELSE 0 END) AS "2", -- 163044
    sum(CASE WHEN genre_count > 2 THEN 1 ELSE 0 END) AS "3+" -- 32109
from (
    select
        record_id, count(*) as genre_count
    from libis_raw_am0.record_genres rg
--    where record_id in (select distinct record_id from libis_raw_am0.orders)
    group by rg.record_id
    order by count(*) desc
) a
;

-- kiek įrašų neturi žanrų
select
    count(*) as "0" -- 85318
from libis_raw_am0.records r
where not exists (
    select 1 from libis_raw_am0.record_genres rg
    where r.id = rg.record_id
);

select
    record_id, count(*) as genre_count
from libis_raw_am0.record_genres rg
--left join libis_raw_am0.records r on r.id = rg.record_id
group by rg.record_id
order by count(*) desc
;

select
--    count(*)
--    *
    count(distinct record_id)
from libis_raw_am0.record_genres rg;

select
    count(distinct record_id)
from libis_raw_am0.orders rg

select *
from libis_raw_am0.records r
join libis_raw_am0.record_genres rg on r.id = rg.record_id;

select count(*) from libis_raw_am0.record_genres rg
where genre = 'undefined';


select count(*) from (select distinct record_id from libis_raw_am0.orders) o
where o.record_id not in (select distinct rg.record_id
from libis_raw_am0.record_genres rg);

-- žanrai ir įrašų kiekiai
select genre, count(genre)
from libis_raw_am0.record_genres
group by genre;

-- įrašai pagal kalbas
select resource_language, count(resource_language)
from libis_raw_am0.record_resource_languages
group by resource_language
order by count(resource_language) desc;

select count(*)
from libis_raw_am0.record_resource_languages
where resource_language not in (
'lit',
'rus',
'eng',
'pol',
'deu',
'fra',
'lat',
'ita',
'lav',
'heb',
'yid',
'nor',
'spa',
'bel'
);

-- įrašai turintys bent vieną perskaitymą pagal žanrų skaičių
select count(*) from libis_raw_am0.records; -- 1107587
select
    sum(CASE WHEN resource_language_count > 0 and resource_language_count <= 1 THEN 1 ELSE 0 END) AS "1", -- 1032001
    sum(CASE WHEN resource_language_count > 1 and resource_language_count <= 2 THEN 1 ELSE 0 END) AS "2", -- 58342
    sum(CASE WHEN resource_language_count > 2 THEN 1 ELSE 0 END) AS "3+" -- 12859
from (
    select
        record_id, count(*) as resource_language_count
    from libis_raw_am0.record_resource_languages rg
--    where record_id in (select distinct record_id from libis_raw_am0.orders)
    group by rg.record_id
    order by count(*) desc
) a
;

-- kiek įrašų neturi kalbos
select
    count(*) as "0" -- 4385
from libis_raw_am0.records r
where not exists (
    select 1 from libis_raw_am0.record_resource_languages rg
    where r.id = rg.record_id
);

-- įrašai pagal išleidimo metus
select release_date, count(release_date)
from libis_raw_am0.records
group by release_date
order by count(release_date) desc;


select
    sum(CASE WHEN release_date >= 2010 THEN 1 ELSE 0 END) AS "2010-2022",
    sum(CASE WHEN release_date >= 2000 and release_date < 2010 THEN 1 ELSE 0 END) AS "2000-2010",
    sum(CASE WHEN release_date >= 1990 and release_date < 2000 THEN 1 ELSE 0 END) AS "1990-2000",
    sum(CASE WHEN release_date >= 1980 and release_date < 1990 THEN 1 ELSE 0 END) AS "1980-1990",
    sum(CASE WHEN release_date >= 1970 and release_date < 1980 THEN 1 ELSE 0 END) AS "1970-1980",
    sum(CASE WHEN release_date >= 1960 and release_date < 1970 THEN 1 ELSE 0 END) AS "1960-1970",
    sum(CASE WHEN release_date >= 1950 and release_date < 1960 THEN 1 ELSE 0 END) AS "1950-1960",
    sum(CASE WHEN release_date >= 1940 and release_date < 1950 THEN 1 ELSE 0 END) AS "1940-1940",
    sum(CASE WHEN release_date >= 1930 and release_date < 1940 THEN 1 ELSE 0 END) AS "1930-1940",
    sum(CASE WHEN release_date >= 1920 and release_date < 1930 THEN 1 ELSE 0 END) AS "1920-1930",
    sum(CASE WHEN release_date >= 1910 and release_date < 1920 THEN 1 ELSE 0 END) AS "1910-1920",
    sum(CASE WHEN release_date >= 1900 and release_date < 1910 THEN 1 ELSE 0 END) AS "1900-1910",
    sum(CASE WHEN release_date >= 1800 and release_date < 1900 THEN 1 ELSE 0 END) AS "1800-1900",
    sum(CASE WHEN release_date < 1800 THEN 1 ELSE 0 END) AS "-1800"
from libis_raw_am0.records
order by count(release_date) desc;