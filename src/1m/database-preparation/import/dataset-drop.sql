drop table libis_1m.country;
drop table libis_1m.county;
drop table libis_1m.education;
drop table libis_1m.identification_code_type;
drop table libis_1m.occupation;
drop table libis_1m.reader_group;


drop table libis_1m.orders;
drop table libis_1m.record_genres;
drop table libis_1m.record_resource_languages;
drop table libis_1m.records;
drop table libis_1m.users;