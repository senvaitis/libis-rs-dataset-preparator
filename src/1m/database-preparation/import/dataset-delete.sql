delete from libis_1m.country;
delete from libis_1m.county;
delete from libis_1m.education;
delete from libis_1m.identification_code_type;
delete from libis_1m.occupation;
delete from libis_1m.reader_group;

TRUNCATE TABLE libis_1m.orders RESTART IDENTITY;
TRUNCATE TABLE libis_1m.record_genres RESTART IDENTITY;
TRUNCATE TABLE libis_1m.record_resource_languages RESTART IDENTITY;
TRUNCATE TABLE libis_1m.records RESTART IDENTITY;
TRUNCATE TABLE libis_1m.users RESTART IDENTITY;