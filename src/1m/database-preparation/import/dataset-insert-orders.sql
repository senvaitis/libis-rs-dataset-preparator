insert into libis_1m.orders (libis_id, creation_date_time, record_id, user_id)
select
    o.id,
    o.creation_date_time,
    r.id,
    u.id
from libis_raw_am0.orders o
join libis_1m.records r on r.libis_id = o.record_id
join libis_1m.users u on u.libis_id = o.user_id
order by o.creation_date_time desc, o.id desc;

delete from libis_1m.records
where id in (
    SELECT id
    FROM libis_1m.records
    EXCEPT
    SELECT distinct record_id
    FROM libis_1m.orders
)