-- 9000 most popular books (am0)
insert into libis_1m.records (
    libis_id,
    code,
    leader,
    title_for_list,
    release_date)
select
    r.id,
    r.code,
    r.leader,
    r.title_for_list,
    release_date
from libis_raw_am0.records r
where r.id in (
    select record_id
    from libis_raw_am0.orders o
    group by record_id
    order by count(*) desc, record_id desc
    limit 9000
--    limit 4000
);
