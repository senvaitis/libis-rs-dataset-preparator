psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        id,
        libis_id,
        case
          when age >= 90 then '90+'
          when age >= 80 then '80-89'
          when age >= 70 then '70-79'
          when age >= 60 then '60-69'
          when age >= 50 then '50-59'
          when age >= 40 then '40-49'
          when age >= 30 then '30-39'
          when age >= 18 then '18-29'
          when age >= 15 then '15-17'
          when age >= 12 then '12-14'
          when age >= 9 then '9-11'
          when age >= 6 then '6-8'
          when age >= 3 then '3-5'
          when age >= 0 then '0-2'
        end as age_group,
        country,
        county,
        education,
        gender,
        identification_code_type,
        is_currently_studying,
        occupation
    from libis_1m.users u
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\1m\u.user' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"
Read-Host -Prompt "Press Enter to exit"