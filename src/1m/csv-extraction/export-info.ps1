psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select count(*), 'users' from libis_1m.users
    union all
    select count(*), 'records' from libis_1m.records
    union all
    select count(*), 'orders' from libis_1m.orders
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\1m\u.info' DELIMITER ',' CSV ENCODING 'UTF8';"
Read-Host -Prompt "Press Enter to exit"