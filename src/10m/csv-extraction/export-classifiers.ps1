psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_10m.country
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\10m\u.country' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_10m.county
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\10m\u.county' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_10m.education
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\10m\u.education' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_10m.identification_code_type
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\10m\u.identification_code_type' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_10m.occupation
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\10m\u.occupation' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

Read-Host -Prompt "Press Enter to exit"