psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select count(*), 'users' from libis_10m.users
    union all
    select count(*), 'records' from libis_10m.records
    union all
    select count(*), 'orders' from libis_10m.orders
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\10m\u.info' DELIMITER ',' CSV ENCODING 'UTF8';"
Read-Host -Prompt "Press Enter to exit"