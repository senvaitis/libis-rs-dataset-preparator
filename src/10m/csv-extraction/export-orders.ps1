psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        o.user_id,
        o.record_id,
        date_trunc('second', o.creation_date_time) as timestamp
    from libis_10m.orders o
    order by o.user_id asc, o.record_id asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\10m\u.order' DELIMITER ',' CSV HEADER;"
Read-Host -Prompt "Press Enter to exit"