insert into libis_10m.country (
    code,
    title)
select
    e.code,
    e.title
from libis_raw_am0.country e
where e.code in (
    select distinct u.country
    from libis_10m.users u
    where u.country is not null
)
order by e.code asc;
