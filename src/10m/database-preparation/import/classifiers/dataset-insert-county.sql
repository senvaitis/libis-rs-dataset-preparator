insert into libis_10m.county (
    code,
    title)
select
    e.code,
    e.title
from libis_raw_am0.county e
where e.code in (
    select distinct u.county
    from libis_10m.users u
    where u.county is not null
)
order by e.code asc;
