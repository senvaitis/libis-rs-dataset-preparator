drop table libis_10m.country;
drop table libis_10m.county;
drop table libis_10m.education;
drop table libis_10m.identification_code_type;
drop table libis_10m.occupation;
drop table libis_10m.reader_group;


drop table libis_10m.orders;
drop table libis_10m.record_genres;
drop table libis_10m.record_resource_languages;
drop table libis_10m.records;
drop table libis_10m.users;