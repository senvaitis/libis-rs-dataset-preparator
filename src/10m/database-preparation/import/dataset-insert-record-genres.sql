insert into libis_10m.record_genres (
    record_id,
    genre)
select distinct
    r.id,
    rg.genre
from libis_raw_am0.record_genres rg
join libis_10m.records r on r.libis_id = rg.record_id;
