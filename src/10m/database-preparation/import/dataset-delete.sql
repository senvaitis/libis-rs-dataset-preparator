delete from libis_10m.country;
delete from libis_10m.county;
delete from libis_10m.education;
delete from libis_10m.identification_code_type;
delete from libis_10m.occupation;
delete from libis_10m.reader_group;

TRUNCATE TABLE libis_10m.orders RESTART IDENTITY;
TRUNCATE TABLE libis_10m.record_genres RESTART IDENTITY;
TRUNCATE TABLE libis_10m.record_resource_languages RESTART IDENTITY;
TRUNCATE TABLE libis_10m.records RESTART IDENTITY;
TRUNCATE TABLE libis_10m.users RESTART IDENTITY;