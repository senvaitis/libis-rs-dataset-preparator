select count(*), 'country' from libis_5m.country
union all
select count(*), 'county' from libis_5m.county
union all
select count(*), 'education' from libis_5m.education
union all
select count(*), 'identification_code_type' from libis_5m.identification_code_type
union all
select count(*), 'occupation' from libis_5m.occupation
union all
select count(*), 'orders' from libis_5m.orders
union all
select count(*), 'reader_group' from libis_5m.reader_group
union all
select count(*), 'record_genres' from libis_5m.record_genres
union all
select count(*), 'record_resource_languages' from libis_5m.record_resource_languages
union all
select count(*), 'records' from libis_5m.records
union all
select count(*), 'users' from libis_5m.users;

-- must be empty
SELECT id
FROM libis_5m.records
EXCEPT
SELECT distinct record_id
FROM libis_5m.orders;
-- must be empty
SELECT id
FROM libis_5m.users
EXCEPT
SELECT distinct user_id
FROM libis_5m.orders;

select * from libis_100k.users;
select * from libis_100k.orders;
select * from libis_100k.records;

-- ranked most reading users
select user_id, count(*) from libis_5m.orders o
group by user_id
order by count(*) desc;

-- verify record count from orders perspective
select count(*) from (
    select distinct record_id from libis_5m.orders
) a;

-- verify user count from orders perspective
select count(*) from (
    select distinct user_id from libis_5m.orders
) a;

-- ranked most read books
select record_id, r.title_for_list, count(*) from libis_5m.orders o
left join libis_5m.records r on r.id = o.record_id
group by record_id, r.title_for_list
order by count(*) desc;

select count(*) from libis_raw_am0.users
where 1=1
--    and country is null
--    and county is null
--    and education is null
--    and identification_code_type is null
--    and occupation is null
    and reader_group is null
;