drop table libis_5m.country;
drop table libis_5m.county;
drop table libis_5m.education;
drop table libis_5m.identification_code_type;
drop table libis_5m.occupation;
drop table libis_5m.reader_group;


drop table libis_5m.orders;
drop table libis_5m.record_genres;
drop table libis_5m.record_resource_languages;
drop table libis_5m.records;
drop table libis_5m.users;