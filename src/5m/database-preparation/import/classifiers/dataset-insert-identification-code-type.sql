insert into libis_5m.identification_code_type (
    code,
    title)
select
    e.code,
    e.title
from libis_raw_am0.identification_code_type e
where e.code in (
    select distinct u.identification_code_type
    from libis_5m.users u
    where u.identification_code_type is not null
)
order by e.code asc;
