insert into libis_5m.record_resource_languages (
    record_id,
    resource_language)
select distinct
    r.id,
    rg.resource_language
from libis_raw_am0.record_resource_languages rg
join libis_5m.records r on r.libis_id = rg.record_id;
