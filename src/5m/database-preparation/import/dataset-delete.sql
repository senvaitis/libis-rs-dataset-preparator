delete from libis_5m.country;
delete from libis_5m.county;
delete from libis_5m.education;
delete from libis_5m.identification_code_type;
delete from libis_5m.occupation;
delete from libis_5m.reader_group;

TRUNCATE TABLE libis_5m.orders RESTART IDENTITY;
TRUNCATE TABLE libis_5m.record_genres RESTART IDENTITY;
TRUNCATE TABLE libis_5m.record_resource_languages RESTART IDENTITY;
TRUNCATE TABLE libis_5m.records RESTART IDENTITY;
TRUNCATE TABLE libis_5m.users RESTART IDENTITY;