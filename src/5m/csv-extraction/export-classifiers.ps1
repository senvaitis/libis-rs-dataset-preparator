psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_5m.country
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\5m\u.country' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_5m.county
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\5m\u.county' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_5m.education
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\5m\u.education' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_5m.identification_code_type
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\5m\u.identification_code_type' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_5m.occupation
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\5m\u.occupation' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

Read-Host -Prompt "Press Enter to exit"