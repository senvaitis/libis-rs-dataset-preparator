CREATE TABLE libis_100k.users (
  id SERIAL,
  libis_id int8 not null unique,
  age int4,
  gender VARCHAR(50),
  is_currently_studying boolean,
  education VARCHAR(50),
  occupation VARCHAR(50),
  country VARCHAR(50),
  county VARCHAR(50),
  identification_code_type VARCHAR(50),
  reader_group VARCHAR(50),
  PRIMARY KEY (id)
);

CREATE TABLE libis_100k.orders (
  id SERIAL,
  libis_id int8 not null unique,
  creation_date_time timestamp,
  user_id int8 not null,
  record_id int8 not null,
  PRIMARY KEY (id)
);

CREATE TABLE libis_100k.records (
  id SERIAL,
  libis_id int8 not null unique,
  code VARCHAR(50),
  leader VARCHAR(50),
  release_date int4,
  title_for_list VARCHAR(4000),
  PRIMARY KEY (id)
);

CREATE TABLE libis_100k.record_genres (
  record_id int8,
  genre VARCHAR(400),
  PRIMARY KEY (record_id, genre)
);

CREATE TABLE libis_100k.record_resource_languages (
  record_id int8,
  resource_language VARCHAR(3),
  PRIMARY KEY (record_id, resource_language)
);

CREATE TABLE libis_100k.country (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

CREATE TABLE libis_100k.county (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

CREATE TABLE libis_100k.education (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

CREATE TABLE libis_100k.identification_code_type (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

CREATE TABLE libis_100k.occupation (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

CREATE TABLE libis_100k.reader_group (
  code VARCHAR(100),
  title VARCHAR(1000),
  PRIMARY KEY (code)
);

alter table if exists libis_100k.orders add constraint fk_orders_user_id foreign key (user_id) references libis_100k.users;
alter table if exists libis_100k.orders add constraint fk_orders_record_id foreign key (record_id) references libis_100k.records;

CREATE INDEX orders_user_index ON libis_100k.orders(user_id);
CREATE INDEX orders_record_index ON libis_100k.orders(record_id);
