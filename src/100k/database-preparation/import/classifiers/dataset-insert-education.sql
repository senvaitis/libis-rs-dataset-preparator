insert into libis_100k.education (
    code,
    title)
select
    e.code,
    e.title
from libis_raw_am0.education e
where e.code in (
    select distinct u.education
    from libis_100k.users u
    where u.education is not null
)
order by e.code asc;
