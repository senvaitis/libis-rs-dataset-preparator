insert into libis_100k.occupation (
    code,
    title)
select
    e.code,
    e.title
from libis_raw_am0.occupation e
where e.code in (
    select distinct u.occupation
    from libis_100k.users u
    where u.occupation is not null
)
order by e.code asc;
