-- with most recent 1M orders on
--   * 2K most popular books
--   * by readers who read at least 20 of those popular books (and less than 500)
-- select first 1060 users
with fresh_orders as (
    select o.id, o.creation_date_time, o.user_id, o.record_id, user_read_count.user_alltime_book_count
    from libis_raw_am0.orders o
    join libis_100k.records r on r.libis_id = o.record_id
    join (
        select user_id, count(*) as user_alltime_book_count from libis_raw_am0.orders o
        join libis_100k.records r on r.libis_id = o.record_id
        group by user_id
        having count(*) >= 20 and count(*) < 500
        order by count(*), user_id desc
    ) user_read_count on user_read_count.user_id = o.user_id
    order by o.creation_date_time desc, o.id desc
--    limit 1000000
)
insert into libis_100k.users (
    libis_id,
    age,
    country,
    county,
    education,
    gender,
    identification_code_type,
    is_currently_studying,
    occupation,
    reader_group
)
select
    u.id,
    u.age,
    u.country,
    u.county,
    u.education,
    u.gender,
    u.identification_code_type,
    u.is_currently_studying,
    u.occupation,
    u.reader_group
from (
    select distinct fo.user_id
    from fresh_orders fo
) last_active_users
join libis_raw_am0.users u on u.id = last_active_users.user_id
where 1=1
    and u.country is not null
    and u.county is not null
    and u.education is not null
    and u.identification_code_type is not null
    and u.occupation is not null
    and u.reader_group is not null
order by last_active_users.user_id desc
limit 1130;

