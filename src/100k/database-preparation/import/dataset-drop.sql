drop table libis_100k.country;
drop table libis_100k.county;
drop table libis_100k.education;
drop table libis_100k.identification_code_type;
drop table libis_100k.occupation;
drop table libis_100k.reader_group;


drop table libis_100k.orders;
drop table libis_100k.record_genres;
drop table libis_100k.record_resource_languages;
drop table libis_100k.records;
drop table libis_100k.users;