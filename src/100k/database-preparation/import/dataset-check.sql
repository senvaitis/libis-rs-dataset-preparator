select count(*), 'country' from libis_100k.country
union all
select count(*), 'county' from libis_100k.county
union all
select count(*), 'education' from libis_100k.education
union all
select count(*), 'identification_code_type' from libis_100k.identification_code_type
union all
select count(*), 'occupation' from libis_100k.occupation
union all
select count(*), 'orders' from libis_100k.orders
union all
select count(*), 'reader_group' from libis_100k.reader_group
union all
select count(*), 'record_genres' from libis_100k.record_genres
union all
select count(*), 'record_resource_languages' from libis_100k.record_resource_languages
union all
select count(*), 'records' from libis_100k.records
union all
select count(*), 'users' from libis_100k.users;

-- must be empty
SELECT id
FROM libis_100k.records
EXCEPT
SELECT distinct record_id
FROM libis_100k.orders;
-- must be empty
SELECT id
FROM libis_100k.users
EXCEPT
SELECT distinct user_id
FROM libis_100k.orders;

select * from libis_100k.users;
select * from libis_100k.orders;
select * from libis_100k.records;

-- ranked most reading users
select user_id, count(*) from libis_100k.orders o
group by user_id
order by count(*) desc;

-- verify record count from orders perspective
select count(*) from (
    select distinct record_id from libis_100k.orders
) a;

-- verify user count from orders perspective
select count(*) from (
    select distinct user_id from libis_100k.orders
) a;

-- ranked most read books
select record_id, r.title_for_list, count(*) from libis_100k.orders o
left join libis_100k.records r on r.id = o.record_id
group by record_id, r.title_for_list
order by count(*) desc;

select user_id, record_id, count(*)
from libis_100k.orders
group by user_id, record_id
order by count(*) desc
;