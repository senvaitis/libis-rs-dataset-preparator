delete from libis_100k.country;
delete from libis_100k.county;
delete from libis_100k.education;
delete from libis_100k.identification_code_type;
delete from libis_100k.occupation;
delete from libis_100k.reader_group;

TRUNCATE TABLE libis_100k.orders RESTART IDENTITY;
TRUNCATE TABLE libis_100k.record_genres RESTART IDENTITY;
TRUNCATE TABLE libis_100k.record_resource_languages RESTART IDENTITY;
TRUNCATE TABLE libis_100k.records RESTART IDENTITY;
TRUNCATE TABLE libis_100k.users RESTART IDENTITY;