psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_100k.country
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\100k\u.country' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_100k.county
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\100k\u.county' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_100k.education
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\100k\u.education' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_100k.identification_code_type
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\100k\u.identification_code_type' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_100k.occupation
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\100k\u.occupation' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        code,
        title
    from libis_100k.reader_group
    order by code asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\100k\u.reader_group' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        distinct genre
    from libis_100k.record_genres
    order by genre asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\100k\u.genre' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"

Read-Host -Prompt "Press Enter to exit"