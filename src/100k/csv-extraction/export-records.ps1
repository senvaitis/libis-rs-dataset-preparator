psql -h localhost -p 5432 -U postgres --password -d libis_rs -c  "\COPY (
    select
        id,
        code,
        leader,
        release_date as release_date,
        title_for_list as title,
        string_agg(rg.genre, '|') as genres
    from libis_100k.records r
    left join libis_100k.record_genres rg on rg.record_id = r.id
    group by r.id
    order by r.id asc
) TO 'C:\Users\kazim\Sources\uni\libis-rs-dataset-preparator\data\100k\u.record-piped' DELIMITER ',' CSV HEADER ENCODING 'UTF8';"
Read-Host -Prompt "Press Enter to exit"